import * as React from 'react';
import {connect} from 'react-redux';
import {Dispatch} from 'redux';
import {IActionType} from '../common';
import {Actions} from '../Actions/Actions';
import {IStoreState} from '../Reducers/Reducers';
import './App.less';
import {Login} from './Login';

/**
 * Пропсы компонента из стора.
 * @prop {boolean} loginStatus Состояние зарегистрированности пользователя.
 * @prop {boolean} waitingForLogin Ожидание завершения процедуры авторизации (завершение логина).
 * @prop {boolean} countResult Результат вычисления.
 * @prop {boolean} counting Выполнение вычисления.
 */
interface IStateProps{
    loginStatus: boolean;
    waitingForLogin: boolean;
    countResult: number;
    counting: boolean;
}

/**
 * Пропсы для передачи экшенов.
 * @prop {Actions} actions Экшены для работы приложения.
 */
export interface IDispatchProps{
    actions: Actions;
}

/**
 * Итоговые пропсы компонента
 */
type TProps = IStateProps & IDispatchProps;

/**
 * Основной класс приложения.
 */
class App extends React.Component<TProps, {}> {

     render() {
        // const {loginStatus, waitingForLogin, countResult, counting} = this.props;
        return (
           <div>
               <Login />
           </div>
        );
    }
}

function mapStateToProps(state: IStoreState): IStateProps {
    return {
        loginStatus: state.loginStatus,
        waitingForLogin: state.loading,
        countResult: state.counter,
        counting: state.counterIsLoading,
    };
}

function mapDispatchToProps(dispatch: Dispatch<IActionType>): IDispatchProps {
    return {
        actions: new Actions(dispatch)
    };
}

const connectApp = connect(mapStateToProps, mapDispatchToProps)(App);

export {connectApp as App};
