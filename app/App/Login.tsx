import * as React from 'react';
import {connect} from 'react-redux';
import {Dispatch} from 'redux';
import {IActionType} from '../common';
import {Actions} from '../Actions/Actions';
import {IStoreState} from '../Reducers/Reducers';

class Login extends React.Component{
    state = {
		name : '',
		pass : '',
        login : false,
	};

    /**
    * Обработчик события изменения инпутов логина и пароля
    */
	handleChange = (e) => {
		const {id, value} = e.currentTarget;
		this.setState({[id] : value });
	};

	/**
	* Обработчик нажатия кнопок логин и логаут
	*/

	handleBtnClick = (e) => {
	    const {id} = e.currentTarget;
	    if( id === 'loginBtn' ){
	        this.setState({ login : true});
	     };
        if( id === 'logoutBtn' ){
            this.setState({
                            name : '',
                            pass : '',
                            login : false,
                          });
        };
    };

    validate = () => {
        const {name, pass} = this.state;
        if( name.trim() && pass.trim()) {
            return true;
        }
        return false;
	};
	render (){
		const {name, pass, login} = this.state;
		return (
			<div className="form-signin">
                <h3 className="form-signin-heading">
                    { !login ? 'Please sign in:' : 'You signed in.' }
                </h3>
                { !login ?
                    <div>
                    <input
                        id="name"
                        type="text"
                        className="form-control"
                        placeholder="Your login"
                        value={name}
                        onChange={this.handleChange}
                    />
                    < input
                    id = "pass"
                    type = "password"
                    className = "form-control"
                    placeholder = "Your password"
                    value = {pass}
                    onChange = {this.handleChange}
                    />
                    </div>
                : ''}
                <div className="loginBtns">
                    { !login ?
                        <input
                            id="loginBtn"
                            className="btn btn-outline-primary"
                            disabled={ !(!login && this.validate())  }
                            type="button"
                            value="Login"
                            onClick={this.handleBtnClick}
                        />
                    : ""}
                    { login ?
						<input
                            id = "logoutBtn"
							className="btn btn-outline-warning"
							disabled={!login}
							type="button"
                            value = "Logout"
							onClick={this.handleBtnClick}
                         />
                        : ''}
                </div>
			</div>
		)
	};
};

export {Login};
